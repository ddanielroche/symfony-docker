# Select image from https://hub.docker.com/_/php/
FROM php:8.1

# Install packages required by PHP extensions
RUN apt-get update -yqq
RUN apt-get install -yqq git unzip libpq-dev libcurl4-gnutls-dev libicu-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev libonig-dev libzip-dev libsodium-dev libxslt1-dev

# Install PHP extensions
RUN docker-php-ext-install session intl sodium ctype xml xsl zip gd pdo_pgsql pdo_mysql mysqli mbstring curl bz2 opcache

#Install & enable redis
RUN pecl install redis
RUN docker-php-ext-enable redis

#Install & enable Xdebug for code coverage reports
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

# Install Symfony Cli
RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | bash
RUN apt-get install -y symfony-cli
#RUN symfony book:check-requirements

# Install Node.js 16.x & yarn
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash
RUN apt-get install -y nodejs
RUN npm install --global yarn
